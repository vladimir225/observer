let instance = null;

class Observer {
  constructor() {
    if (instance) {
      return instance;
    }
    instance = this;
    this._callbacks = {};
  }

  on(type, cb) {
    if (!this._callbacks[type]) {
      this._callbacks[type] = [];
    }
    this._callbacks[type].push(cb);
    console.log(this._callbacks);
  }

  fire({ type, data }) {
    if (this._callbacks[type]) {
      this._callbacks[type].forEach(item => {
        item(data);
      });
    }
  }

  off(type, cb) {
    if (this._callbacks[type]) {
      this._callbacks[type] = this._callbacks[type].filter(item => item !== cb);
    }
  }
}
